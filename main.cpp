#include <iostream>
#include <fstream>
#include <cmath>
#include <string>

using namespace std;

//Objekt  Masse_g  Kantenlaenge_cm_Umfang_ausen Hoehe_cm_Radius_innen
double wuerfeldat[]={ 388,  7.95,    0};
double zylinderdat[]={ 342,  7.9,    7.9};
double kugeldat[]={ 360,  31.9,    5.1};
double hohlzyllinder[]={260,  9.9,    8.6};
double hanteldat[]={ 55,  40.2,    0,
0,   56,  0,    0,
0,   39,  0,    0};
double stabdat[]={ 170,  49.9,    24.9,
0,  0,  0,     17};

//Messreihe_Gewichte_fuer_Federkostante
//Radius der SCheibe
double radiusscheibe = (16.3/2)/100;
//Masse_g Auslenkung_Grad
double gew10 = 30;
double gew15 = 45;
double gew20 =  60;
double gew25 =  70;
double gew35 =  105;
double gew50 = 160;
//Gegenrichtung
double ggew10 = -25;
double ggew15 = -40;
double ggew20 = -55;
double ggew25 = -70;
double ggew35 = -100;
double ggew50 = -150;

//Schwingungen_der_einzelnen_Koerper
//Objekt   Schwingungen Zeit_s
double kugel[]={  10,  9.03,  9.31,  9.10};
double zylinder[]={  10,  8.22,  8.15,  8.31};
double hohlzylinder[]={  15,  16.60,  16.31,  16.54};
double hantel[]={  15,  51.09,  51.32,  51.37};
double wuerfel_Flaeche[]={ 15,  15.13,  14.81,  14.69};
double wuerfel_Ecke[]={  15,  14.69,  14.53,  14.66};
double stab_0_2[]={  15,  37.87,  37.59,  37.56};
double stab_7_8[]={  15,  42.07,  41.97,  42.00};
double tischchen_0[]={  20,  17.38,  17.41,  17.41};
double tischchen_15[]={  15,  13.53,  13.65,  13.53};
double tischchen_30[]={  15,  14.41,  14.54,  14.41};
double tischchen_45[]={  15,  15.25,  15.15,  15.18};
double tischchen_60[]={  15,  15.75,  15.94,  16.00};
double tischchen_75[]={  15,  15.81,  16.13,  15.78};
double tischchen_90[]={  15,  15.62,  15.72,  15.53};
double tischchen_105[]={ 15,  15.03,  14.88,  14.94};
double tischchen_120[]={ 15,  14.09,  14.25,  14.22};
double tischchen_135[]={ 15,  13.59,  13.37,  13.35};
double tischchen_150[]={ 15,  12.90,  12.85,  12.75};
double tischchen_165[]={ 15,  12.97,  12.72,  12.47};
double tischchen_180[]={ 15,  13.00,  13.09,  13.03};

//Zweiter_Teil
double speichendurchmesser= 42.5;// 0.2
double gewicht=   254;
double gewichtposition=  17; //0.5
double kleine_Scheibe=  12.9; //0.5
double schwingung_Position_1[]={
25.72,  26.35,  26.06};
double schwingung_Position_2[]={
25.90,  26.03,  26.15};

//Gewichte_fallen_am_Rad_g Abstande_cm
double r100[]={ 0.0, 0.5, 0.6, 0.8, 0.9, 1.0, 1.2, 1.3, 1.5, 1.8, 1.9, 2.0, 2.1, 2.3, 2.4, 2.6, 2.7, 2.8, 2.9, 3.0, 3.1, 3.2, 3.3, 3.5, 3.6, 3.8, 4.0, 4.2, 4.3, 4.4, 4.6, 4.7, 4.8, 4.9, 5.0, 5.1, 5.2, 5.3, 5.5};

double r200[]={ 0.0, 0.7, 0.9, 1.2, 1.6, 1.9, 2.2, 2.6, 2.9, 3.2, 3.5, 3.3, 4.0, 4.3, 4.6, 4.9, 5.2, 5.5, 5.8, 6.1, 6.4, 6.7, 7.0, 7.3, 7.6, 7.9, 8.2, 8.5};

double r500[]={ 0.0, 3.0, 3.2, 3.1, 4.4, 5.3, 6.0, 6.9, 7.7, 8.5, 9.4, 10.3, 11.1, 12.0, 12.8, 13.6};

double r1000[]={0.0, 3.3, 4.0, 5.7, 7.5, 8.2, 10.7, 12.5, 14.1, 15.8, 17.4, 18.9};

//Winkelrichtgroesse auchtung D durch linear Fit muss noch 1 durch D
//D = 3.23 +- 0.13
//D = 3.11 +-0.04
const double D  = 0.0144;
const double DF = 0.0006;

//Beschleunigung a durch regression
double a100 = 0.1368;
double a100f = 0.0016;

double a200 = 0.304;
double a200f = 0.003;

double a500 = 0.84;
double a500f = 0.03;

double a1000 = 1.67;
double a1000f =0.04;

//Funktionen
void Winkelricht();
void thetat(double korp[], double & mittel,double & stand, double & the, double & thets);
void tischchen(double korpe[], double & mitt, double &sta, double & tthe, double & tthets, double & polar, double & polarf);
void linregbesch(double mass[], string name, double size);
void thetatrad(double period[], double & mittel, double & stand, double & the, double & thets);
void thetaradfall(double bes, double besf, double rad, double radk, double masse, double & the, double & thets);

int main(){

ofstream out;
Winkelricht();
double mittelwert = 0, standab = 0, theta = 0, thetaf = 0, polardi = 0, polardif = 0;

mittelwert = 0, standab = 0, theta = 0, thetaf = 0;
thetat(&kugel[0],mittelwert,standab,theta,thetaf);
cout << "Kugel\t\t" << mittelwert << " " << standab << "\t" << theta << " " << thetaf << endl;

mittelwert = 0; standab = 0; theta = 0; thetaf = 0;
thetat(&zylinder[0],mittelwert,standab,theta,thetaf);
cout << "Zylinder\t" << mittelwert << " " << standab << "\t" << theta << " " << thetaf << endl;

mittelwert = 0; standab = 0; theta = 0; thetaf = 0;
thetat(&hohlzylinder[0],mittelwert,standab,theta,thetaf);
cout << "Hohlzylinder\t" << mittelwert << " " << standab << "\t" << theta << " " << thetaf << endl;

mittelwert = 0; standab = 0; theta = 0; thetaf = 0;
thetat(&hantel[0],mittelwert,standab,theta,thetaf);
cout << "Hantel\t\t" << mittelwert << " " << standab << "\t" << theta << " " << thetaf << endl;

mittelwert = 0; standab = 0; theta = 0; thetaf = 0;
thetat(&wuerfel_Flaeche[0],mittelwert,standab,theta,thetaf);
cout << "Wuerfel_Flaeche\t" << mittelwert << " " << standab << "\t" << theta << " " << thetaf << endl;

mittelwert = 0; standab = 0; theta = 0; thetaf = 0;
thetat(&wuerfel_Ecke[0],mittelwert,standab,theta,thetaf);
cout << "Wuerfel_Ecke\t" << mittelwert << " " << standab << "\t" << theta << " " << thetaf << endl;

mittelwert = 0; standab = 0; theta = 0; thetaf = 0;
thetat(&stab_0_2[0],mittelwert,standab,theta,thetaf);
cout << "Stab0.2\t\t" << mittelwert << " " << standab << "\t" << theta << " " << thetaf << endl;

mittelwert = 0; standab = 0; theta = 0; thetaf = 0;
thetat(&stab_7_8[0],mittelwert,standab,theta,thetaf);
cout << "Stab7.8\t\t" << mittelwert << " " << standab << "\t" << theta << " " << thetaf << endl;

cout << endl;
out.open("tischchen.txt");

mittelwert = 0; standab = 0; theta = 0; thetaf = 0, polardi = 0, polardif = 0;
tischchen(&tischchen_0[0],mittelwert,standab,theta,thetaf, polardi, polardif);
cout << "Tischchen_0\t\t" << mittelwert << " " << standab << "\t" << theta << " " << thetaf << "\t" << polardi << " " << polardif << endl;
out << "0 " << mittelwert << " " << standab << "\t" << theta << " " << thetaf << "\t" << polardi << " " << polardif << endl;

mittelwert = 0; standab = 0; theta = 0; thetaf = 0, polardi = 0, polardif = 0;
tischchen(&tischchen_15[0],mittelwert,standab,theta,thetaf, polardi, polardif);
cout << "Tischchen_15\t\t" << mittelwert << " " << standab << "\t" << theta << " " << thetaf << "\t" << polardi << " " << polardif << endl;
out << "15 " << mittelwert << " " << standab << "\t" << theta << " " << thetaf << "\t" << polardi << " " << polardif << endl;

mittelwert = 0; standab = 0; theta = 0; thetaf = 0, polardi = 0, polardif = 0;
tischchen(&tischchen_30[0],mittelwert,standab,theta,thetaf, polardi, polardif);
cout << "Tischchen_30\t\t" << mittelwert << " " << standab << "\t" << theta << " " << thetaf << "\t" << polardi << " " << polardif << endl;
out << "30 " << mittelwert << " " << standab << "\t" << theta << " " << thetaf << "\t" << polardi << " " << polardif << endl;

mittelwert = 0; standab = 0; theta = 0; thetaf = 0, polardi = 0, polardif = 0;
tischchen(&tischchen_45[0],mittelwert,standab,theta,thetaf, polardi, polardif);
cout << "Tischchen_45\t\t" << mittelwert << " " << standab << "\t" << theta << " " << thetaf << "\t" << polardi << " " << polardif << endl;
out << "45 " << mittelwert << " " << standab << "\t" << theta << " " << thetaf << "\t" << polardi << " " << polardif << endl;

mittelwert = 0; standab = 0; theta = 0; thetaf = 0, polardi = 0, polardif = 0;
tischchen(&tischchen_60[0],mittelwert,standab,theta,thetaf, polardi, polardif);
cout << "Tischchen_60\t\t" << mittelwert << " " << standab << "\t" << theta << " " << thetaf << "\t" << polardi << " " << polardif << endl;
out << "60 " << mittelwert << " " << standab << "\t" << theta << " " << thetaf << "\t" << polardi << " " << polardif << endl;

mittelwert = 0; standab = 0; theta = 0; thetaf = 0, polardi = 0, polardif = 0;
tischchen(&tischchen_75[0],mittelwert,standab,theta,thetaf, polardi, polardif);
cout << "Tischchen_75\t\t" << mittelwert << " " << standab << "\t" << theta << " " << thetaf << "\t" << polardi << " " << polardif << endl;
ot << "75 " << mittelwert << " " << standab << "\t" << theta << " " << thetaf << "\t" << polardi << " " << polardif << endl;

mittelwert = 0; standab = 0; theta = 0; thetaf = 0, polardi = 0, polardif = 0;
tischchen(&tischchen_90[0],mittelwert,standab,theta,thetaf, polardi, polardif);
cout << "Tischchen_90\t\t" << mittelwert << " " << standab << "\t" << theta << " " << thetaf << "\t" << polardi << " " << polardif << endl;
out << "90 " << mittelwert << " " << standab << "\t" << theta << " " << thetaf << "\t" << polardi << " " << polardif << endl;

mittelwert = 0; standab = 0; theta = 0; thetaf = 0, polardi = 0, polardif = 0;
tischchen(&tischchen_105[0],mittelwert,standab,theta,thetaf, polardi, polardif);
cout << "Tischchen_105\t\t" << mittelwert << " " << standab << "\t" << theta << " " << thetaf << "\t" << polardi << " " << polardif << endl;
out << "105 " << mittelwert << " " << standab << "\t" << theta << " " << thetaf << "\t" << polardi << " " << polardif << endl;

mittelwert = 0; standab = 0; theta = 0; thetaf = 0, polardi = 0, polardif = 0;
tischchen(&tischchen_120[0],mittelwert,standab,theta,thetaf, polardi, polardif);
cout << "Tischchen_120\t\t" << mittelwert << " " << standab << "\t" << theta << " " << thetaf << "\t" << polardi << " " << polardif << endl;
out << "120 " << mittelwert << " " << standab << "\t" << theta << " " << thetaf << "\t" << polardi << " " << polardif << endl;

mittelwert = 0; standab = 0; theta = 0; thetaf = 0, polardi = 0, polardif = 0;
tischchen(&tischchen_135[0],mittelwert,standab,theta,thetaf, polardi, polardif);
cout << "Tischchen_135\t\t" << mittelwert << " " << standab << "\t" << theta << " " << thetaf << "\t" << polardi << " " << polardif << endl;
out << "135 " << mittelwert << " " << standab << "\t" << theta << " " << thetaf << "\t" << polardi << " " << polardif << endl;

mittelwert = 0; standab = 0; theta = 0; thetaf = 0, polardi = 0, polardif = 0;
tischchen(&tischchen_150[0],mittelwert,standab,theta,thetaf, polardi, polardif);
cout << "Tischchen_150\t\t" << mittelwert << " " << standab << "\t" << theta << " " << thetaf << "\t" << polardi << " " << polardif << endl;
out << "150 " << mittelwert << " " << standab << "\t" << theta << " " << thetaf << "\t" << polardi << " " << polardif << endl;

mittelwert = 0; standab = 0; theta = 0; thetaf = 0, polardi = 0, polardif = 0;
tischchen(&tischchen_165[0],mittelwert,standab,theta,thetaf, polardi, polardif);
cout << "Tischchen_165\t\t" << mittelwert << " " << standab << "\t" << theta << " " << thetaf << "\t" << polardi << " " << polardif << endl;
out << "165 " << mittelwert << " " << standab << "\t" << theta << " " << thetaf << "\t" << polardi << " " << polardif << endl;

mittelwert = 0; standab = 0; theta = 0; thetaf = 0, polardi = 0, polardif = 0;
tischchen(&tischchen_180[0],mittelwert,standab,theta,thetaf, polardi, polardif);
cout << "Tischchen_180\t\t" << mittelwert << " " << standab << "\t" << theta << " " << thetaf << "\t" << polardi << " " << polardif << endl;
out << "180 " << mittelwert << " " << standab << "\t" << theta << " " << thetaf << "\t" << polardi << " " << polardif << endl;
/*
//andere Halbachse
mittelwert = 0; standab = 0; theta = 0; thetaf = 0, polardi = 0, polardif = 0;
tischchen(&tischchen_0[0],mittelwert,standab,theta,thetaf, polardi, polardif);
cout << "Tischchen_0\t\t" << mittelwert << " " << standab << "\t" << theta << " " << thetaf << "\t" << -1*polardi << " " << polardif << endl;
out << "0 " << mittelwert << " " << standab << "\t" << theta << " " << thetaf << "\t" << -polardi << " " << -polardif << endl;

mittelwert = 0; standab = 0; theta = 0; thetaf = 0, polardi = 0, polardif = 0;
tischchen(&tischchen_15[0],mittelwert,standab,theta,thetaf, polardi, polardif);
cout << "Tischchen_15\t\t" << mittelwert << " " << standab << "\t" << theta << " " << thetaf << "\t" << -1*polardi << " " << polardif << endl;
out << "15 " << mittelwert << " " << standab << "\t" << theta << " " << thetaf << "\t" << -polardi << " " << -polardif << endl;

mittelwert = 0; standab = 0; theta = 0; thetaf = 0, polardi = 0, polardif = 0;
tischchen(&tischchen_30[0],mittelwert,standab,theta,thetaf, polardi, polardif);
cout << "Tischchen_30\t\t" << mittelwert << " " << standab << "\t" << theta << " " << thetaf << "\t" << -1*polardi << " " << polardif << endl;
out << "30 " << mittelwert << " " << standab << "\t" << theta << " " << thetaf << "\t" << -polardi << " " << -polardif << endl;

mittelwert = 0; standab = 0; theta = 0; thetaf = 0, polardi = 0, polardif = 0;
tischchen(&tischchen_45[0],mittelwert,standab,theta,thetaf, polardi, polardif);
cout << "Tischchen_45\t\t" << mittelwert << " " << standab << "\t" << theta << " " << thetaf << "\t" << -1*polardi << " " << polardif << endl;
out << "45 " << mittelwert << " " << standab << "\t" << theta << " " << thetaf << "\t" << -polardi << " " << -polardif << endl;

mittelwert = 0; standab = 0; theta = 0; thetaf = 0, polardi = 0, polardif = 0;
tischchen(&tischchen_60[0],mittelwert,standab,theta,thetaf, polardi, polardif);
cout << "Tischchen_60\t\t" << mittelwert << " " << standab << "\t" << theta << " " << thetaf << "\t" << -1*polardi << " " << polardif << endl;
out << "60 " << mittelwert << " " << standab << "\t" << theta << " " << thetaf << "\t" << -polardi << " " << -polardif << endl;

mittelwert = 0; standab = 0; theta = 0; thetaf = 0, polardi = 0, polardif = 0;
tischchen(&tischchen_75[0],mittelwert,standab,theta,thetaf, polardi, polardif);
cout << "Tischchen_75\t\t" << mittelwert << " " << standab << "\t" << theta << " " << thetaf << "\t" << -1*polardi << " " << polardif << endl;
out << "75 " << mittelwert << " " << standab << "\t" << theta << " " << thetaf << "\t" << -polardi << " " << -polardif << endl;

mittelwert = 0; standab = 0; theta = 0; thetaf = 0, polardi = 0, polardif = 0;
tischchen(&tischchen_90[0],mittelwert,standab,theta,thetaf, polardi, polardif);
cout << "Tischchen_90\t\t" << mittelwert << " " << standab << "\t" << theta << " " << thetaf << "\t" << -1*polardi << " " << polardif << endl;
out << "90 " << mittelwert << " " << standab << "\t" << theta << " " << thetaf << "\t" << -polardi << " " << -polardif << endl;

mittelwert = 0; standab = 0; theta = 0; thetaf = 0, polardi = 0, polardif = 0;
tischchen(&tischchen_105[0],mittelwert,standab,theta,thetaf, polardi, polardif);
cout << "Tischchen_105\t\t" << mittelwert << " " << standab << "\t" << theta << " " << thetaf << "\t" << -1*polardi << " " << polardif << endl;
out << "105 " << mittelwert << " " << standab << "\t" << theta << " " << thetaf << "\t" << -polardi << " " << -polardif << endl;

mittelwert = 0; standab = 0; theta = 0; thetaf = 0, polardi = 0, polardif = 0;
tischchen(&tischchen_120[0],mittelwert,standab,theta,thetaf, polardi, polardif);
cout << "Tischchen_120\t\t" << mittelwert << " " << standab << "\t" << theta << " " << thetaf << "\t" << -1*polardi << " " << polardif << endl;
out << "120 " << mittelwert << " " << standab << "\t" << theta << " " << thetaf << "\t" << -polardi << " " << -polardif << endl;

mittelwert = 0; standab = 0; theta = 0; thetaf = 0, polardi = 0, polardif = 0;
tischchen(&tischchen_135[0],mittelwert,standab,theta,thetaf, polardi, polardif);
cout << "Tischchen_135\t\t" << mittelwert << " " << standab << "\t" << theta << " " << thetaf << "\t" << -1*polardi << " " << polardif << endl;
out << "135 " << mittelwert << " " << standab << "\t" << theta << " " << thetaf << "\t" << -polardi << " " << -polardif << endl;

mittelwert = 0; standab = 0; theta = 0; thetaf = 0, polardi = 0, polardif = 0;
tischchen(&tischchen_150[0],mittelwert,standab,theta,thetaf, polardi, polardif);
cout << "Tischchen_150\t\t" << mittelwert << " " << standab << "\t" << theta << " " << thetaf << "\t" << -1*polardi << " " << polardif << endl;
out << "150 " << mittelwert << " " << standab << "\t" << theta << " " << thetaf << "\t" << -polardi << " " << -polardif << endl;

mittelwert = 0; standab = 0; theta = 0; thetaf = 0, polardi = 0, polardif = 0;
tischchen(&tischchen_165[0],mittelwert,standab,theta,thetaf, polardi, polardif);
cout << "Tischchen_165\t\t" << mittelwert << " " << standab << "\t" << theta << " " << thetaf << "\t" << -1*polardi << " " << polardif << endl;
out << "165 " << mittelwert << " " << standab << "\t" << theta << " " << thetaf << "\t" << -polardi << " " << -polardif << endl;

mittelwert = 0; standab = 0; theta = 0; thetaf = 0, polardi = 0, polardif = 0;
tischchen(&tischchen_180[0],mittelwert,standab,theta,thetaf, polardi, polardif);
cout << "Tischchen_180\t\t" << mittelwert << " " << standab << "\t" << theta << " " << thetaf << "\t" << -1*polardi << " " << polardif << endl;
out << "180 " << mittelwert << " " << standab << "\t" << theta << " " << thetaf << "\t" << -polardi << " " << -polardif << endl;
*/
out.close();

//Ausgabe der Daten fuer die beschleunigung
/*
linregbesch( &  r100[0],  "r100", (sizeof(r100) / sizeof(r100[0])) );
linregbesch( &  r200[0],  "r200", (sizeof(r200) / sizeof(r200[0])) );
linregbesch( &  r500[0],  "r500", (sizeof(r500) / sizeof(r500[0])) );
linregbesch( & r1000[0], "r1000", (sizeof(r1000) / sizeof(r1000[0])) );
*/

mittelwert = 0; standab = 0; theta = 0; thetaf = 0;
thetatrad( & schwingung_Position_1[0], mittelwert, standab, theta, thetaf);
cout << "Schwingung_pos_1: \t" << mittelwert << " " << standab << "\t" << theta << " " << thetaf << endl;

mittelwert = 0; standab = 0; theta = 0; thetaf = 0;
thetatrad( & schwingung_Position_2[0], mittelwert, standab, theta, thetaf);
cout << "Schwingung_pos_2: \t" << mittelwert << " " << standab << "\t" << theta << " " << thetaf << endl;

cout << endl;
theta = 0, thetaf = 0;
thetaradfall(a100,a100f,(speichendurchmesser/200),(kleine_Scheibe/200),0.1, theta, thetaf);
cout << "100g: \t" << theta << " " << thetaf << endl;

theta = 0, thetaf = 0;
thetaradfall(a200,a200f,(speichendurchmesser/200),(kleine_Scheibe/200),0.2, theta, thetaf);
cout << "200g: \t" << theta << " " << thetaf << endl;

theta = 0, thetaf = 0;
thetaradfall(a500,a500f,(speichendurchmesser/200),(kleine_Scheibe/200),0.5, theta, thetaf);
cout << "500g: \t" << theta << " " << thetaf << endl;

theta = 0, thetaf = 0;
thetaradfall(a1000,a1000f,(speichendurchmesser/200),(kleine_Scheibe/200),1, theta, thetaf);
cout << "1000g: \t" << theta << " " << thetaf << endl;

return 0;
}

void Winkelricht(){
ofstream out;
out.open("Winkel.txt");
out << 0.01*9.81*radiusscheibe << " " << gew10*M_PI/180 << " " << (ggew10*-1)*M_PI/180 << " " << 3*M_PI/180 << endl;
out << 0.015*9.81*radiusscheibe << " " << gew15*M_PI/180 << " " << (ggew15*-1)*M_PI/180 << " " << 3*M_PI/180 << endl;
out << 0.02*9.81*radiusscheibe << " " << gew20*M_PI/180 << " " << (ggew20*-1)*M_PI/180 << " " << 3*M_PI/180 << endl;
out << 0.025*9.81*radiusscheibe << " " << gew25*M_PI/180 << " " << (ggew25*-1)*M_PI/180 << " " << 3*M_PI/180 << endl;
out << 0.035*9.81*radiusscheibe << " " << gew35*M_PI/180 << " " << (ggew35*-1)*M_PI/180 << " " << 3*M_PI/180 << endl;
out << 0.05*9.81*radiusscheibe << " " << gew50*M_PI/180 << " " << (ggew50*-1)*M_PI/180 << " " << 3*M_PI/180 << endl;
out.close();
}


void thetat(double korp[], double & mittel,double & stand, double & the, double & thets){
mittel = 0;
stand = 0;
for(int i = 1; i <4; i++){
mittel += korp[i];
}
mittel /=3;
//cout << mittel << " ";
for(int i = 1; i < 4; i++){
stand += pow(korp[i]-mittel,2);
}
stand /= sqrt(stand/(6));
mittel /=korp[0];
stand /= korp[0];
//cout << stand << endl;
the = mittel*mittel*D/(4*M_PI*M_PI);
//Fehler von Theta nicht vergessen
thets = sqrt(
 //Fehler T
 stand*stand*
 pow((2*mittel*D)/(4*M_PI*M_PI),2)
 +
 //Fehler D
 DF*DF*
 pow((mittel*mittel)/(4*M_PI*M_PI),2)
);
}

void tischchen(double korpe[], double & mitt, double &sta, double & tthe, double & tthets, double &polar, double & polarf){
mitt = 0;
sta = 0;
for(int i = 1; i <4; i++){
mitt += korpe[i];
}
mitt /=3;
//cout << mittel << " ";
for(int i = 1; i < 4; i++){
sta += pow(korpe[i]-mitt,2);
}
sta /= sqrt(sta/(6));
mitt /=korpe[0];
sta /= korpe[0];

tthe = mitt*mitt*D/(4*M_PI*M_PI);
//Fehler von Theta nicht vergessen
tthets = sqrt(
 //Fehler T
 sta*sta*
 pow((2*mitt*D)/(4*M_PI*M_PI),2)
 +
 //Fehler D
 DF*DF*
 pow((mitt*mitt)/(4*M_PI*M_PI),2)
);
polar = 1/sqrt(tthe);
//Fehler polar
polarf = sqrt(
tthets*tthets*
pow(-0.5*pow(tthe,-1.5),2)
);
}

void linregbesch(double mass[], string name, double size){
string datei;
//double test[] = { 6, 6, 5, 6, 6};
datei = name +".txt";
ofstream ausg(datei.c_str());
double strecke = 0;
cout << size << endl;
for(int i = 0; i < size; i++){
// strecke +=mass[i];
 ausg << i*0.1 << " " << mass[i]/10;
 if(datei == "r100.txt" || datei == "r200.txt"){
  ausg << " 0.001" << endl;
 }
 else{
  ausg << " 0.002" << endl;
 }
}
ausg.close();
}

void thetatrad(double period[], double & mittel, double & stand, double & the, double & thets){
mittel = 0;
stand = 0;
for(int i = 0; i < 3; i++){
mittel += period[i];
}
mittel /=3;
//cout << mittel << " ";
for(int i = 0; i < 3; i++){
stand += pow(period[i]-mittel,2);
}
stand /= sqrt(stand/(6));
mittel /= 10;
stand  /= 10;
//cout << stand << endl;
the = mittel*mittel*9.81*(gewicht/1000)*(gewichtposition/100)/(4*M_PI*M_PI) - (gewicht/1000)*(gewichtposition/100)*(gewichtposition/100);
//Fehler von Theta nicht vergessen
thets = sqrt(
 //Fehler T
 stand*stand*
 pow((2*mittel*D)/(4*M_PI*M_PI),2)
 +
 //Fehler D
 DF*DF*
 pow((mittel*mittel)/(4*M_PI*M_PI),2)
);
}

void thetaradfall(double bes, double besf, double rad, double radk, double masse, double & the, double & thets){

the = (rad*radk*masse*9.81)/bes - masse*radk*radk;
thets=sqrt(
 //Fehler r
 0.005*0.005*
 pow((rad*masse*9.81)/bes-masse*2*radk,2)
 +
 //Fehler R
 0.002*0.002*
 pow((radk*masse*9.81)/bes,2)
 +
 //Fehler Masse
 0.0001*0.0001*
 pow((rad*radk*9.81)/bes-radk*radk,2)
 +
 //Fehler a
 besf*besf*
 pow(((-1)*rad*radk*masse*9.81)/(bes*bes),2));
}
