reset
set terminal epslatex color
set output 'tischchenpolar.tex'
#set terminal png
#set output 'tischchen.png'

set xlabel 'Drehung des Tischchen [Grad]'
set ylabel 'reziproke Quadratwurzeln $1/\sqrt{\Theta}$'

set grid polar
set polar
set angles degrees
set dummy t
#set yrange [0:60]
#set xrange [-70:70]

set fit logfile 'tischchenfit.log'
#f(t,y)=t**2/a**2+y**2/b**2+c
#f(t,y)=(t*cos(y)+c)**2/a**2+(t*cos(y))**2/b**2
#f(t)=(a)/(1+b*cos(t))
#f(t)=a*t**2+b*t+c

#fit f(t) 'tischchen.txt' u 1:6:7 via a,b,c

#set yrange [0:60]
#set xrange [-70:70]
p 'tischchen.txt' u 1:6:7 t 'reziproke Quadratwurzeln' w e

set output
