% Für Bindekorrektur als optionales Argument "BCORfaktormitmaßeinheit", dann
% sieht auch Option "twoside" vernünftig aus
% Näheres zu "scrartcl" bzw. "scrreprt" und "scrbook" siehe KOMA-Skript Doku
\documentclass[12pt,a4paper,titlepage,headinclude,bibtotoc]{scrartcl}

%---- Allgemeine Layout Einstellungen ------------------------------------------

% Für Kopf und Fußzeilen, siehe auch KOMA-Skript Doku
\usepackage[komastyle]{scrpage2}
\pagestyle{scrheadings}
\setheadsepline{0.5pt}[\color{black}]
\automark[section]{chapter}

%Einstellungen für Figuren- und Tabellenbeschriftungen
\setkomafont{captionlabel}{\sffamily\bfseries}
\setcapindent{0em}


%---- Weitere Pakete -----------------------------------------------------------
% Die Pakete sind alle in der TeX Live Distribution enthalten. Wichtige Adressen
% www.ctan.org, www.dante.de

% Sprachunterstützung
\usepackage[ngerman]{babel}

% Benutzung von Umlauten direkt im Text
% entweder "latin1" oder "utf8"
\usepackage[utf8]{inputenc}

% Pakete mit Mathesymbolen und zur Beseitigung von Schwächen der Mathe-Umgebung
\usepackage{latexsym,exscale,stmaryrd,amssymb,amsmath}

% Weitere Symbole
\usepackage[nointegrals]{wasysym}
\usepackage{eurosym}

% Anderes Literaturverzeichnisformat
%\usepackage[square,sort&compress]{natbib}

% Für Farbe
\usepackage{color}

% Zur Graphikausgabe
%Beipiel: \includegraphics[width=\textwidth]{grafik.png}
\usepackage{graphicx}

% Text umfließt Graphiken und Tabellen
% Beispiel:
% \begin{wrapfigure}[Zeilenanzahl]{"l" oder "r"}{breite}
%   \centering
%   \includegraphics[width=...]{grafik}
%   \caption{Beschriftung} 
%   \label{fig:grafik}
% \end{wrapfigure}
\usepackage{wrapfig}

% Mehrere Abbildungen nebeneinander
% Beispiel:
% \begin{figure}[htb]
%   \centering
%   \subfigure[Beschriftung 1\label{fig:label1}]
%   {\includegraphics[width=0.49\textwidth]{grafik1}}
%   \hfill
%   \subfigure[Beschriftung 2\label{fig:label2}]
%   {\includegraphics[width=0.49\textwidth]{grafik2}}
%   \caption{Beschriftung allgemein}
%   \label{fig:label-gesamt}
% \end{figure}
\usepackage{subfigure}

% Caption neben Abbildung
% Beispiel:
% \sidecaptionvpos{figure}{"c" oder "t" oder "b"}
% \begin{SCfigure}[rel. Breite (normalerweise = 1)][hbt]
%   \centering
%   \includegraphics[width=0.5\textwidth]{grafik.png}
%   \caption{Beschreibung}
%   \label{fig:}
% \end{SCfigure}
\usepackage{sidecap}

% Befehl für "Entspricht"-Zeichen
\newcommand{\corresponds}{\ensuremath{\mathrel{\widehat{=}}}}

%Fußnoten zwingend auf diese Seite setzen
\interfootnotelinepenalty=1000

%Für chemische Formeln (von www.dante.de)
%% Anpassung an LaTeX(2e) von Bernd Raichle
\makeatletter
\DeclareRobustCommand{\chemical}[1]{%
  {\(\m@th
   \edef\resetfontdimens{\noexpand\)%
       \fontdimen16\textfont2=\the\fontdimen16\textfont2
       \fontdimen17\textfont2=\the\fontdimen17\textfont2\relax}%
   \fontdimen16\textfont2=2.7pt \fontdimen17\textfont2=2.7pt
   \mathrm{#1}%
   \resetfontdimens}}
\makeatother


\begin{document}

\begin{titlepage}
\centering
\textsc{\Large Anfängerpraktikum der Fakultät für
  Physik,\\[1.5ex] Universität Göttingen}

\vspace*{4.2cm}

\rule{\textwidth}{1pt}\\[0.5cm]
{\huge \bfseries
  Versuch Trägheitsmomente\\[1.5ex]
  Protokoll}\\[0.5cm]
\rule{\textwidth}{1pt}

\vspace*{3.5cm}

\begin{Large}
\begin{tabular}{ll}
Praktikant: &  Michael Lohmann\\
 &  Kevin Lüdemann\\
E-Mail: & m.lohmann@stud.uni-goettingen.de\\
 & kevin.luedemann@stud.uni-goettingen.de\\
Betreuer: & Martin Ochmann\\
\end{tabular}
\end{Large}

\vspace*{0.8cm}

\begin{Large}
\fbox{
  \begin{minipage}[t][2.5cm][t]{6cm} 
    Testat:
  \end{minipage}
}
\end{Large}

\end{titlepage}

\tableofcontents

\newpage

\section{Einleitung}
\label{sec:einleitung}

Trägheitsmomente spielen im Alltag eine große Rolle.
So gibt es zum Beispiel keinen Motor, der keine rotierenden Bauteile besitzt.
Deshalb ist es von zentraler Bedeutung, die Trägheitsmomente von verschiedenen Körpern möglichst genau zu vermessen.
Für einige spezielle haben wir dies getan.

\section{Theorie}
\label{sec:theorie}

\subsection{Trägheitsmomente}


Die Geschwindigkeit eines Massepunktes in einem rotierenden Körper lässt sich schreiben als
\begin{align}
\vec{v}=\vec{\omega}\times \vec{r}
\end{align}
Wobei $\vec{\omega}$ die Winkelgeschwindigkeit entlang einer bestimmten Achse ist, die durch den Schwerpunkt des Körpers verläuft und $\vec{r}$ der Ortsvektor des Massepunktes bezüglich des Schwerpunkts.\\
Die Rotationsenergie eines starren Körpers mit der Masse M berechnet sich durch
\begin{align}
E_{rot}=\sum\limits_i\dfrac{1}{2}m_i\vec{v}_i\,^2=\sum\limits_i\dfrac{1}{2}m_i(\vec{\omega}\times\vec{r}_i)^2
\end{align}
Oder in der diskretisierten Form
\begin{align}
E_{rot}=\dfrac{1}{2}\int\limits_{M} (\vec{\omega}\times\vec{r})^2dm
\end{align}
Wenn man auf die Schreibweise des Kreuzproduktes verzichten möchte, kann man bloß den senkrechten Teil des Abstandes zur Drehachse betrachten
\begin{align}
E_{rot}=\dfrac{1}{2}\vec{\omega}\,^2\int\limits_{M} \vec{r}_\perp^{\: 2}\text{d} m
\end{align}
Nun definiert man das Trägheitsmoment bezüglich der Drehachse $A$ (mit $r_\perp \perp A$) als
\begin{align}
I_A=\int\limits_M \vec{r}_\perp^{\: 2}\text{d} m\label{eq:traegmom}
\end{align}
Daraus folgt $\left(\text{mit } \vec{\omega}\parallel A\right)$
\begin{align*}
E_{rot}=\dfrac{1}{2}I_A\vec{\omega}^2
\end{align*}\\
Dies auch im Gerthsen\footnote{Gerthsen, ISBN:978-3-642-12893-6, Springer Verlag, 24. Auflage} auf Seite 80 nachzulesen.
\subsection{Der Steiner'sche Satz}
Bisher haben wir nur mit einer Rotation um eine Achse durch den Schwerpunkt des Körpers rechnen können.
Eine Verallgemeinerung davon ist der Satz von Steiner, welcher nach Demtröder\footnote{Demtröder Seite: 146, ISBN: 978-3-540-79294-9, Springer Verlag, 5. Auflage} besagt, dass man jede beliebige Achse $A'$ berechnen kann, indem man es einfach für die Achse $A\parallel A'$ tut und die Masse mal das Abstandsquadrat $Md^2$ hinzu addiert, wie man in Abb. \ref{fig:steiner} sehen kann.
\begin{figure}[!h]
\centering
\includegraphics[width=0.4\linewidth]{Steiner.png}
\caption{Satz von Steiner\label{fig:steiner}}
\end{figure}


\subsection{Hauptträgheitsachsen}
Jeder Körper hat sogenannte Hauptträgheitsachsen\footnote{ebd. Seite 154}.
Diese zeichnen sich dadurch aus, dass das Trägheitsmoment bezüglich dieser Achsen eine Diagonalmatrix wird.
Dies sind die Achsen, um die ein Körper ohne Unwucht rotieren kann.
Man findet sie, indem man die Rotation um alle Achsen betrachtet und dann die Maxima und Minima der Trägheitsmomente findet.
Sie liegen immer senkrecht aufeinander, was bedeutet, dass in diesem Bezugssystem die Matrix des Trägheitsmomentes so aussieht: $$I=\left(\begin{array}{ccc}
I_a & 0 & 0 \\ 
0 & I_b & 0 \\ 
0 & 0 & I_c
\end{array}\right)$$
Besitzt ein Körper eine Symmetrieachse, so verläuft durch diese eine Hauptträgheitsachse hindurch.
Deshalb sind zum Beispiel auch die Trägheitsmomente des Würfels $ I_{\text{Würfel}\perp}=I_\text{Würfel\_E}$ identisch.

\subsection{Analogien zwischen Translation und Rotation}
Dem aufmerksamen Leser fallen mit Sicherheit Analogien zu der meist bekannteren Translation auf.
So ist z.B. die Translationsenergie $E_{trans}=\frac{1}{2}m\vec{v}^2$ von der Struktur her sehr ähnlich zur Rotationsenergie $E_{rot}=\frac{1}{2}I\vec{\omega}^2$.
Nun stellt sich natürlich die Frage, ob sich weitere Ähnlichkeiten finden lassen.
Die Antwort lautet ja und eben diese sind in der folgenden Tabelle~\ref{tab:analogie} zu finden: 

\renewcommand{\arraystretch}{1.5}
\begin{table}[!h]
\centering
\begin{tabular}{|l|r||l|r|}
\hline 
\multicolumn{2}{|c||}{Translation} & \multicolumn{2}{|c|} {Rotation} \\ 
\hline\hline
Ort & $\vec{r}$ & Winkel & $\vec{\varphi}$ \\ 
\hline 
Geschwindigkeit & $\vec{v}=\dot{\vec{r}}$ & Winkelgeschwindigkeit & $\vec{\omega}=\dot{\vec{\varphi}}$ \\ 
\hline 
Beschleunigung & $\vec{a}=\dot{\vec{v}}$ & Winkelbeschleunigung & $\ddot{\vec{\varphi}} $\\
\hline
Masse & $m$ & Trägheitsmoment& $I$\\
\hline
Impuls & $\vec{p}=m\vec{v}$ & Drehimpuls & $\vec{L}=I\vec{\omega}$\\
\hline
Kraft & $\vec{F}=\dot{\vec{p}}$ & Drehmoment & $\vec{M}=\dot{\vec{L}}$\\
\hline
\end{tabular} 
\caption{Analogien zwischen Translation und Rotation\label{tab:analogie}}

\end{table}
\renewcommand{\arraystretch}{1}

\subsection{Spezielle Trägheitsmomente}
Für die Auswertung dieses Versuches ist die Kenntnis verschiedener spezieller Trägheitsmomente nötig, welche in der Tabelle \ref{tbl:traegmom} zu finden sind.

\renewcommand{\arraystretch}{1.5}
\begin{table}[!h]
 \centering
\begin{tabular}{|l|l|}
\hline Objekt & Trägheitsmoment\\\hline
\hline massive Kugel & $I_{\text{Kugel}}=\frac{2}{5}m\cdot r^2=\frac{1}{10\pi^2}m\cdot U^2$\\
\hline massiver Zylinder & $I_{\text{m\_Zylinder}}=\frac{1}{2}m\cdot r^2$\\
\hline hohler Zylinder & $I_{\text{h\_Zylinder}}= m\cdot \frac{r_a^2+r_i^2}{2}$\\
\hline Würfel mit $\vec{\omega}\perp$ Oberfläche & $I_{\text{Würfel}\perp}=\frac{1}{6}m\cdot a^2$\\
\hline Würfel mit $\vec{\omega}$ durch Ecke & $I_{\text{Würfel\_E}}=\frac{1}{6}m\cdot a^2$\\
\hline Scheibe & $I_\text{Scheibe}=\frac{1}{2}mr^2$\\
\hline Stange durch CM & $I_\text{Stange\_CM}=\frac{1}{12}mr^2$\\
\hline Stange versetzt & $I_\text{Stange\_v}=\frac{1}{12}mr^2+md^2$\\
\hline Hantel & $I_\text{Hantel}=I_\text{Stab}+2I_\text{Ende}\approx\frac{1}{12}m_\text{Stab}\cdot l^2+\frac{l^2}{2}m_\text{Ende}$\\\hline
\end{tabular}
\caption{Ausgewählte Trägheitsmomente\label{tbl:traegmom}}
\end{table}
\renewcommand{\arraystretch}{1}


\subsection{Trägheitsmoment beschleunigter Räder}
\subsubsection{Fallendes Gewicht}
Das Trägheitsmoment eines Rades wie in Abb. \ref{fig:fall}, welches durch ein fallendes Gewicht beschleunigt wird:\\
Auf das fallende Gewicht wirkt natürlich die Schwerkraft $F_g=mg$. Auf den Faden wirkt diese Kraft, allerdings nicht ganz, da das Rad ja ''nachgibt''.
Auf den Faden wirkt also die Kraft $F_\text{Seil}=m(g-a')$.
Das auf das Rad wirkende Drehmoment ist demnach $M=r\cdot F_\text{Seil}$.
Das Drehmoment ist allerdings ebenfalls gleich $M=I\cdot\ddot\varphi$.\\
$\Rightarrow\: I=\frac{m(g-a')r}{\ddot\varphi}$\\
Nach dem Strahlensatz gilt: $a'=a\cdot\frac{r}{R}$ und aus $\omega R=v$ folgt mit der Ableitung $\ddot\varphi R=a$\\
\begin{align}\Rightarrow\: I=\frac{mgrR}{a}-mr^2\label{eq:Ia}\end{align} 

\subsubsection{Physikalisches Pendel}
Das Drehmoment $M=-rF\sin\varphi=-rmg\sin\varphi$  ist gleich dem Trägheitsmoment $I\ddot\varphi$\\
$\Rightarrow$ mit der Kleinwinkelnäherung ($\sin\varphi\approx\varphi$): $I\ddot\varphi+rmg\varphi=0$\\
Nach Physik 1 gilt nun: $I=\frac{mgsT^2}{4\pi^2}$, woraus mit Hilfe des Hebelgesetzes für den Schwerpunkt $s=\frac{0\cdot M+zm_z}{M+m_z}=\frac{zm_z}{M+m_z}$ und dem Satz von Steiner folgt
\begin{align}
I=\frac{(M+m_z)gsT^2}{4\pi^2}-mz^2=\frac{gzm_zT^2}{4\pi^2}-mz^2\label{eq:Ias}
\end{align}

\section{Durchführung}
\label{sec:durchfuehrung}

\subsection{Teil A}
Zu Beginn dieses Versuches muss man alle wichtigen Daten für die spätere Auswertung notieren.
Dies wären: Massen der einzelnen Körper, so wie deren Ausdehnung.\\
Danach muss man die Winkelrichtgröße der Feder bestimmen.
Dies tut man, indem man für 6 kleine Gewichte zwischen 10g und 50g die Auslenkung der Scheibe misst, wenn man sie, wie in Abb.~\ref{fig:richtgr} gezeigt, so anhängt, dass die Drehachse horizontal verläuft.
Dabei ist natürlich darauf zu achten, dass das Gewicht nicht zu groß ist, so dass die Feder irgendwo anstößt.\\

Als nächstes richtet man das Rad auf, um im weiteren Versuchsverlauf den störenden Einfluss der Gravitation zu minimieren.
Man schraubt nun die Probekörper auf das Schwungrad und misst die Schwingungsdauer von 10-20 Schwingungen pro Körper je 3 mal.
Hier bei ist ebenfalls darauf zu achten, dass die Feder nicht an den Anschlag kommt.\\
Als letzten Teilversuch montiert man das "Tischlein", was eine eigene drehbare Achse hat, wodurch man sein Trägheitsmoment verändern kann.
Auch hierbei geht man wie im Versuch zuvor vor, nur dass man nach Beendigung eines Versuches das Tischlein um 15$^\circ$ weiter dreht, bis man eine halbe Umdrehung vollendet hat. 

 \begin{figure}[htb]
   \centering
   \subfigure[Bestimmung der Richtgröße\label{fig:richtgr}]
   {\includegraphics[width=0.43\textwidth]{richtgr.eps}}
   \hfill
   \subfigure[Fallversuch\label{fig:fall}]
   {\includegraphics[width=0.46\textwidth]{fall.eps}}
   \caption{Versuchsaufbauten}
   \label{fig:versuchsaufbauten}
 \end{figure}


\subsection{Teil B}
Man zeichnet die Daten des Schwungrades, welches in Abb. \ref{fig:fall} zu sehen ist auf. Diese wären: der Radius des großen, sowie des kleinen Rades, und die Länge und Masse des Zusatzgewichtes für den zweiten Teil.
Zu Beginn lässt man an dem kleinen Rad mit Hilfe eines Fadens befestigt Gewichte mit den Massen 0.1, 0.2, 0.5, und 1 kg fallen.
Ein um das äußere Schwungrad befestigter Papierstreifen dabei im Abstand von 0.1 s von einem Marker mit einem Strich versehen.
Dabei ist darauf zu achten, dass das Rad nach einer Umdrehung aufgehalten wird, da es sonst bei der Auswertung schwieriger wird, die einzelnen Umdrehungen auseinander zu halten.
Nach jeder Messung verschiebt man den Papierstreifen ein kleines Stück, damit man die Markierungen der unterschiedlichen Messungen voneinander unterscheiden kann. \\\\
Für den zweiten Teil der Messung wird das Schwungrad als physikalisches Pendel ''umgebaut''. Dafür hängt man an eine der Speichen ein Gewicht.
Dann wird das Rad ein kleines Stückchen ausgelenkt und 3 mal die Zeit genommen, die es für 10 Schwingungen bei kleiner Auslenkung benötigt.
Anschließend führt man den selben Versuch noch einmal durch, nur mit dem Gewicht an einer gegenüber liegenden Speiche.\\\\


\section{Auswertung}
\label{sec:auswertung}
Die Auswertung wurde teilweise mithilfe eines selbstgeschriebenen c++ Programms durchgeführt, welches die Fehlerrechnung und Mittelung der Daten übernimmt, sowie die meisten der Berechnungen.
Dieses Programm befindet sich im Anhang (\ref{ag:prog}).
Die von uns aufgenommenen Daten befinden sich in der Globalen Variablen Deklaration des Programms.\\
Wir haben für alle Längenmessungen einen Fehler von 1mm und für Massen von 1g angenommen.


\subsection{Winkelrichtgröße D}
Die Winkelrichtgröße wurde mithilfe der einer linearen Regression bestimmt.
Die hierfür verwendeten Daten sind die, die durch die senkrecht stehende Feder und die angreifenden Gewichtskräfte gemessen.
Hier raus ergab sich der Plot gezeigt in Abbildung (\ref{img:D}).
\begin{figure}[!h]
	\centering
		\input{winkelfit.tex}
	\caption{lineare Regression der Winkelrichtgröße in beide Richtungen}\label{img:D}
\end{figure}
Aus der linearen Regression ergeben sich Werte für die Steigung m in der linken Richtung von $(71\pm3)Nm^{-1}$ 
und in der rechten Richtung von $(68.0\pm0.9)Nm^{-1}$.
Hierbei ist zu beachten, das für die Winkelrichtgröße D gilt $D=-\frac{M}{\varphi}$ ist wobei M das Drehmoment und $\varphi$ die Auslenkung ist.
Somit D der Kehrwert der Steigung m der Regressionsgeraden.
Der Fehler wurde mithilfe der Gauß'schen Fehlerfortpflanzung umgerechnet.
\begin{align}
	\sigma_D = \sqrt{\sigma_m^2 \cdot\left(\frac{1}{m^2}\right)^2}
\end{align}
Für die Winkelrichtgröße aus der Steigung ergaben sich die Werte aus Tabelle (\ref{tbl:m}).
%links  m = 71 \pm 3 chisquare     = 1.8
%rechts m = 68.0 \pm 0.9 chisqaura = 0.2
\begin{table}[!h]
	\centering
	\begin{tabular}{|c|c|c|}
		\hline
	&	links [Nm] & rechts [Nm]\\
		\hline
	D &	($0.0141\pm 0.0006$) & ($0.0147\pm 0.0002$)\\
		\hline
	\end{tabular}	
	\caption{Werte für D in Nm abhängig von der Richtung der Auslenkung}\label{tbl:m}
\end{table}
Um weiter rechnen zu können mittel wir D und kommen auf diesen endgültigen Wert mit dem größeren Fehler der beiden, da die Standartabweichung einen zu kleinen statistischen Fehler ausgeben würde.
\begin{align}
	\bar{D} = (0.0144\pm0.0006)Nm
\end{align}
Im Folgenden wird der Einfachheit halber $\bar{D}$ mit D bezeichnet.

\subsection{Trägheitsmoment aus Periodendauer}
\label{sec:tht}
Das Trägheitsmoment aus der Periodendauer wird nach der Formel \eqref{eq:traegmom} berechnet. Die für die Fehler nötige Formel wird aus der Gauß'schen Fehlerfortpflanzung berechnet.
Es wird die Annahme gemacht, das die Periodendauer und die Winkelrichtgröße einen Fehler haben.
Somit ergibt sich diese Fehlerformel.
\begin{align}
	\sigma_{\Theta} = \sqrt{\sigma_T^2\cdot\left(\frac{2T\cdot D}{4\pi^2}\right)^2+\sigma_D^2\cdot\left(\frac{T^2}{4\pi^2}\right)^2}
\end{align}
Die Periodendauer wurde durch Mitteln der 3 Messungen pro Körper und dividieren durch die Anzahl der Schwingungen errechnet.
Hierbei wird nur der Statistische Fehler angenommen.
Somit ergeben sich für die einzelnen Körper die in Tabelle \ref{tbl:thetat} angegeben Werte für die Periodendauer T und $\Theta$.
\begin{table}[!h]
	\centering
	\begin{tabular}{|c|c|c|}
	\hline
	Körper & Periodendauer  [s]  & $\Theta$ [kg$\cdot$ m$^2$]\\
	\hline
	Kugel	 	& $0.91\pm0.05$		& $0.00031\pm0.00004$\\
	\hline
	Zylinder	& $0.82\pm0.03$		& $0.00025\pm0.00002$\\
	\hline
	Hohlzylinder	& $1.10\pm0.04$		& $0.00044\pm0.00004$\\
	\hline
	Hantel		& $3.42\pm0.04$		& $0.0043\pm0.0002$\\
	\hline
	Würfel Fläche	& $0.99\pm0.06$		& $0.00036\pm0.00005$\\
	\hline
	Würfel Ecke	& $0.98\pm0.02$		& $0.00035\pm0.00003$\\
	\hline
	Stab im CM	& $2.51\pm0.04$		& $0.00230\pm0.00013$\\
	\hline
	Stab versetzt	& $2.80\pm0.02$		& $0.00286\pm0.00013$\\
	\hline
	\end{tabular}
	\caption{Trägheitsmomente der einzelnen Körper aus der Periodendauer\label{tbl:thetat}}
\end{table}
Diese Werte für $\Theta$ werden unter Punkt \ref{sec:verg} mit den aus den Körpereigenschaften berechneten $\Theta$ Werten verglichen.

\subsection{Theoretisches Trägheitsmoment}
\label{sec:thfor}
Für das theoretische Trägheitsmoment mit den gegebenen Größen ergeben sich die Werte der Tabelle \ref{tbl:thetafor}.
Die Fehler wurde jeweils mithilfe der Gauß'schen Fehlerfortpflanzung anhand der Formeln aus Tabelle \ref{tbl:traegmom}
 berechnet.
Es wurde hierbei angenommen, das jeweils die Massen und die gemessenen Längen, Umfänge, Höhen und Breiten einen Fehler haben.
\begin{align}
	\sigma_{I_\text{Kugel}} =& \sqrt{\sigma_m^2\cdot\left(\frac{U^2}{10\pi^2}\right)^2+\sigma_U^2\cdot\left(\frac{2m\cdot U}{10\pi^2}\right)^2}\\
	\sigma_{I_\text{h\_Zylinder}} =& \sqrt{\sigma_m^2\cdot\left(\frac{r_a^2+r_i^2}{2}\right)^2+\sigma_{r_a}^2\cdot\left(\frac{2mr_a}{2}\right)^2+\sigma_{r_i}^2\cdot\left(\frac{2mr_b}{2}\right)^2}\\
	\sigma_{I_\text{Würfel$\perp$}} =& \sqrt{\sigma_m^2\cdot\left(\frac{a^2}{6}\right)^2+\sigma_a^2\cdot\left(\frac{ma}{3}\right)^2}\\
	\sigma_{I_\text{Würfel\_E}} =& \sqrt{\sigma_m^2\cdot\left(\frac{a^2}{6}\right)^2+\sigma_a^2\cdot\left(\frac{ma}{3}\right)^2}\\
	\sigma_{I_\text{Zylinder}} =& \sqrt{\sigma_m^2\cdot\left(\frac12r^2\right)^2+\sigma_r^2\cdot\left(mr\right)^2}\\
	\sigma_{I_\text{Stange\_CM}} =& \sqrt{\sigma_m^2\cdot\left(\frac1{12}r^2\right)^2+\sigma_r^2\cdot\left(\frac16mr\right)^2}\\
	\sigma_{I_\text{Stange\_versetzt}} =& \sqrt{\sigma_m^2\cdot\left(\frac1{12}r^2+d^2\right)^2+\sigma_r^2\cdot\left(\frac16mr\right)^2+\sigma_d^2\cdot\left(2md\right)^2}\\
	\sigma_{I_\text{Hantel}} =& \sqrt{\sigma_{m_{Stab}}^2\cdot\left(\frac{l^2}{12}\right)^2+\sigma_{m_{Ende}}^2\cdot\left(\frac{l^2}2\right)^2+\sigma_l^2\cdot\left(\frac16m_{Stab}\cdot l+lm_{Ende}\right)^2}
\end{align}
Aus den jeweiligen Formeln und dieser Fehlerformeln ergeben sich nun die in Tabelle \ref{tbl:thetafor} angegebenen Werte für $\Theta$.

\begin{table}[!h]
	\begin{tabular}{|c|p{4cm}|p{4cm}|c|}
	\hline
	Körper 		& Längen [m] 			& Massen [kg] 		& $\Theta$ [kg$\cdot$m$^2$]\\
	\hline
	Kugel		& U=(0.319$\pm$0.001)		& $0.360\pm0.001$	& 0.0003712$\pm$0.0000016 \\
	\hline 
	Hohlzylinder	& $r_a$=(0.099$\pm$0.001), $r_i$=(0.086$\pm$0.001) 	&0.260$\pm$0.001  & 0.00224$\pm$0.00004\\
	\hline
	Würfel Fläche	& a=(0.079$\pm$0.001)		& 0.388$\pm$0.001	&0.00040$\pm$0.00002 \\
	\hline
	Würfel Ecke	& a=(0.079$\pm$0.001)		& 0.388$\pm$0.001	&0.00040$\pm$0.00002 \\
	\hline
	Hantel		& l=(0.402$\pm$0.001)		& außen=(0.055$\pm$0.001), Stab=(0.039$\pm$0.001) & 0.00497$\pm$0.00009\\
	\hline
	Zylinder	& h=b=(0.079$\pm$0.001)		& 0.342$\pm$0.001	& 0.00107$\pm$0.00003\\
	\hline
	Stab im CM	& l=(0.499$\pm$0.001),  d\_CM=(0.000$\pm$0.001) & 0.170$\pm$0.001 & 0.001764$\pm$0.000009\\
	\hline
	Stab versetzt	& l=(0.499$\pm$0.001),  d\_CM=(0.079$\pm$0.001) & 0.170$\pm$0.001 & 0.0028$\pm$0.0003\\
	\hline
	\end{tabular}
	\caption{Trägheitsmomente der einzelnen Körper aus den Formeln\label{tbl:thetafor}}
\end{table} 

\subsection{Tischchen}
Zu Auswertung der Daten des Tischchen haben wir mithilfe des c++ Programms für die jeweiligen Gradeinstellungen die Periodendauer gemittelt.
Anschließend, wie oben bei den Trägheitsmomenten der anderen Körper, wurde das jeweilige Trägheitsmoment mithilfe des Ansatzes der Periodendauer ermittelt.
Die sich daraus ergebenen Werte wurde dann weiter verwendet, um die reziproken Quadratwurzeln zu berechnen.
Der Fehler dieser wurde mithilfe der Gauß'schen Fehlerfortpflanzung errechnet.
\begin{align}
	\sigma_{1/\sqrt{\Theta}} = \sqrt{\sigma_{\Theta}^2\cdot\left(-\frac{1}{2\Theta^{\frac32}}\right)^2}
\end{align}
Die Werte wurde anschließend in Polarkoordinaten geplottet.
Das Ergebnis ist in Abbildung \ref{img:tischpol} zu sehen.
Aus den Daten bzw. kann man nun erkennen, das sich die minimale Hauptträgheitsachse bei $(15\pm5)^{\circ}$ und die maximale Hauptträgheitsachse bei $(105\pm5)^{\circ}$ befindet.

\subsection{Trägheitsmoment des Rades aus Fallbeschleunigung}
Die Abstände der Zeitmarken gegen die Zeit aufgetragen für die einzelnen Gewichte 100g, 200g, 500g und 1000g befinden sich in Anhang \ref{img:r100}.
Die jeweiligen lineare Regressionen sind mit ihren Ergebnissen in der Tabelle \ref{tbl:gewfall} eingetragen.
\begin{table}[!hb]
	\centering
	\begin{tabular}{|c|c|c|}
	\hline
	Gewicht [g] 	& $\chi^2$ 	& Steigung a [$\frac{m}{s^2}$]\\
	\hline
	100		& 1.15		& $0.1368\pm0.0016$ \\
	\hline
	200		& 1.32		& $0.304\pm0.003$ \\
	\hline
	500		& 5		& $0.84\pm0.03$ \\
	\hline
	1000		& 5.3		& $1.67\pm0.04$\\
	\hline
	\end{tabular}
	\caption{Ergebnisse der liearen Regression der Beschleunigung der einzelnen Massen\label{tbl:gewfall}}
\end{table}
Aus diesen Steigungen und der Formel \eqref{eq:Ia}
lässt sich jetzt das Trägheitsmoment des Rades bestimmen.
Hierzu sind noch die Werte für den Radius des Rades (0.213$\pm$0.002)m, der Radius des Rades für den Faden (0.065$\pm$0.005)m.
Der Fehler wird wieder per Gauß'scher Fehlerfortpflanzung berechnet.
\begin{align}
	\varsigma_r =& \sigma_r^2\cdot\left(\frac{Rmg}{a}-2mr\right)^2\\
	\varsigma_R =& \sigma_R^2\cdot\left(\frac{rmg}{a}\right)^2\\
	\varsigma_m =& \sigma_m^2\cdot\left(\frac{rRg}{a}-r^2\right)^2\\
	\varsigma_a =& \sigma_a^2\cdot\left(\frac{-rRmg}{a^2}\right)^2\\
	\sigma_{\Theta} =& \sqrt{\varsigma_r+\varsigma_R+\varsigma_m+\varsigma_a}
\end{align}
Somit ergeben sich die Werte für $\Theta$ aus der Tabelle \ref{tbl:thradfall}.
\begin{table}[!ht]
	\centering
	\begin{tabular}{|c|c|}
	\hline
	Gewicht [g]	& $\Theta$ [kg$\cdot$m$^2$]\\
	\hline
	100		& $0.098\pm0.008$  \\
	\hline
	200		& $0.088\pm0.007$\\
	\hline
	500		& $0.078\pm0.007$\\
	\hline
	1000		& $0.076\pm0.006$\\
	\hline
	\end{tabular}
	\caption{Werte für $\Theta$ berechnet aus der Fallbeschleunigung unterschiedlicher Gewichte\label{tbl:thradfall}}
\end{table}

\subsection{Trägheitsmoment des Rades aus physikalischen Pendel}
Durch Verwenden der Formel \eqref{eq:Ias}, lässt sich aus den Schwingungen wieder das Trägheitsmoment bestimmen.
Desweiteren sind noch die Masse m des Zusatzgewichtes (0.254$\pm$0.001)kg bzw. der Abstand z dieses Gewichtes zur Drehachse z (0.170$\pm$0.005)m.
Der Fehler wird wieder mithilfe der Fehlerfortpflanzung berechnet.
\begin{align}
	\varsigma_T =& \sigma_T^2\cdot\left(\frac{2Tgmz}{4\pi^2}\right)^2\\
	\varsigma_m =& \sigma_m^2\cdot\left(\frac{T^2gz}{4\pi^2}-z^2\right)^2\\
	\varsigma_z =& \sigma_z^2\cdot\left(\frac{T^2gm}{4\pi^2}-2mz\right)^2\\
	\sigma_{\Theta} =& \sqrt{\varsigma_T+\varsigma_m+\varsigma_z}
\end{align}
Somit ergeben sich die Werte aus der Tabelle \ref{tbl:thprad} für $\Theta$.
\begin{table}[!h]
	\centering
	\begin{tabular}{|c|c|c|}
	\hline
	Position	& Periode [s]	& $\Theta$ [kg$\cdot$m$^2$] \\
	\hline
	1. Position	& 2.6$\pm$0.2	& 0.0654$\pm$0.0003\\
	\hline
	2. Position	& 2.60$\pm$0.05	& 0.06534$\pm$0.00014\\
	\hline
	\end{tabular}
	\caption{Werte für $\Theta$ aus der Periodendauer des Rades\label{tbl:thprad}}
\end{table}

\section{Diskussion}
\label{sec:diskussion}
\subsection{unterschiedliche Winkelrichtgrößen}
Während unserer Auswertung der Winkelrichtgröße ist aufgefallen, das sich die beiden Werte für links und rechts sehr stark ähneln.
Dies wird besonders deutlich, wenn man den Fehler des Wertes für die Linksauslenkung anschaut.
Es fällt auf, dass der Fehler so groß ist, dass der Wert für rechts noch in diesem Intervall liegt.
Aus diesem Grund ist es möglich von beiden den Mittelwert zu nehmen und damit weiter zu rechnen.
Die macht die weiteren Rechnungen für die Trägheitsmomente der anderen Körper sehr viel einfacher.
Allerdings hätte die Standartabweichung einen viel zu kleinen Fehler wiedergegeben, deswegen haben wir den größten der beiden Fehler angegebenf, welcher immer noch recht klein.
Nun liegen wirklich beide Werte in dem Fehlerintervall des Mittelwertes und wir können ohne Probleme mit diesen gemittelten Wert für die Winkelrichtgröße weiter arbeiten.


\subsection{Vergleich zwischen den $\Theta$ Werten für die Körper}
\label{sec:verg}
Der Vergleich zwischen den theoretisch berechneten und durch die Periode experimentell bestimmten Werten für das Trägheitsmoment der einzelnen Körper zeigt uns die Zuverlässigkeit der experimentellen Methode.
An unseren Daten kann man erkennen, dass z.B. die $\Theta$ Werte der beiden Methoden für die Kugel mit einer Abweichung kleiner 20\% beieinander liegen, aber nicht im Fehlerintervall der anderen Methode liegen.
Das gleiche gilt auch für die Hantel und den Stab im Schwerpunkt und versetzt um die Strecke d mit einer Abweichung kleiner 25\%.
Bei diesen Körpern ist die Methode über die Periodendauer sehr aussagekräftig und kann zum bestimmen des Trägheitsmomentes genommen werden.\\
Besonders gut war die Bestimmung des Trägheitsmomentes des Würfel, ob auf der Ecke, oder der Fläche.
Hier schneiden sich sogar die recht kleinen Fehlerintervalle, auch wenn es für die Ecke nur ein geringer Schnitt ist.
Somit haben wir für 4 Körper eine relativ gute Methode darin gefunden die Trägheitsmomente aus der Schwingungsdauer zu bestimmen, mit einer tragbaren Abweichung von 25\%.\\
Betrachtet man jetzt aber die Werte für den Zylinder und den Hohlzylinder, so liegen die Werte um den Faktor 10 auseinander.
Dies ist ein Fehler, der auf eine fehlerhafte Messung der Schwingungen zurück geführt werden kann.
Beispielsweise kann es passiert sein, dass sich die Feder, welche für die Schwingungen benutzt wurde, beim Schwingen gegen die Wand, oder gegen sich selber gestoßen ist.
Dies führt zu einer nicht mehr linearen Winkelrichtgröße und somit zu falschen Werten.
Dies kann verhindert werden, in dem die Feder nicht zu weit ausgelenkt wird. 
Bzw. indem man darauf achtet Messungen, bei dem die Feder sich berührt, oder gegen die Wand stößt zu wiederholen und somit diese Nichtlinearitäten aus den Daten herraus zu halten.

\subsection{Tischchen}
Die Hauptträgheitsachsen des Tischchens, welche aus dem Plot in Polarkoordinaten der Winkelposition gegen die reziproken Quadratwurzel aufgetragen wurden, haben wir aus nur einer halben Drehung des Tischchens bestimmt.
Dies liegt zum einem daran, das sich für die zweite Hälfte nur die Werte gespiegelt ergebenen hätten und uns zum anderen nicht die Zeit blieb, tatsächlich eine volle Drehung um 2$\pi$ aufzunehmen.
Die kann dazu geführt haben, dass sich die Achsen nicht ganz genau dort befunden haben können, wo wir sie nun bestimmt haben.
Außerdem erhöht sich dadurch der Fehler der einzelnen Werte auf mehre Grad.
Hinzu kommt, dass wir die Ellipse nicht fitten konnten und somit die Werte per Augenmaß ablesen mussten.

\subsection{Vergleich zwischen den $\Theta$ Werte für das Rad}
Bei der Auswertung für Trägheitsmomente des Rades fällt auf, dass die $\Theta$-Werte aus der Periodendauer wieder sehr dicht beieinander liegen.
Dies ist darauf zurück zuführen, dass die Ergebnisse der Periode der beiden verschiedenen Positionen des Zusatzgewichtes im Fehlerintervall des anderen liegen.
Hierraus folgt, das auch die $\Theta$-Werte im Fehlerintervall des anderen liegen.
Hier wird auf das Mittel verzichtet, da die Werte tatsächlich sehr nah beieinander liegen und somit ein Vergleich mit den Werten aus der wirkenden Gewichtskraft durchaus möglich ist.\\
Betrachtet man jetzt die eben angesprochenen Werte, berechnet aus der Fallbeschleunigung der Gewichte, erkennt man schon unter den Werte für die Gewichte, dass sie recht stark schwanken.
Die Fehlerintervalle der Messungen für 1000g, 500g und 200g schneiden sich nur am den äußeren Ränder, was darauf hindeutet, dass mit einem viel größerem Fehler gerechnet werden sollte.
Das Fehlerintervall für 100g schneidet sich hingegen nur sehr knapp mit dem von 200g, was darauf hindeutet, dass für die geringen Beschleunigung durch die Gewichtskraft die Auflösung des Messstreifens nicht groß genug war. 
In diesem Fall lagen die Markierungen zu dicht neben einander und der Apparat hat einen etwas zu breiten Streifen auf dem Papier hinterlassen.
Dies führt zu einem größeren Fehler als angenommen und somit würden dann auch die Fehlerintervalle wieder ineinander liegen.\\
Bei den letzten beiden Gewichten hingegen waren die Markierungen auf dem Papierstreifen durch die große Beschleunigung um etwa 45$^{\circ}$ gedreht.
Dies führte dazu, dass für die Abstände die Länge jedes Streifens auf dem Papier gemittelt werden musste.
Aus diesem Grund ist der Fehler bei diesen beiden Messungen doppelt so groß wie bei den ersten beiden Messungen.\\\\
Vergleicht man jetzt die $\Theta$-Werte aus der Messung der Fallbeschleunigung und der Periode des physikalischen Pendels erkennt man, dass sie immer noch sehr stark voneinander verschieden sind.
Dies liegt an der Ungenauigkeit der Messung der Fallbeschleunigung, wie oben beschrieben.
Aus diesen Unterschieden kann man folgern, dass die Messung des Trägheitsmomentes eines Rades über die Periodendauer deutlich einfacher und fehlerfreier ist.
Dies liegt vor allem daran, dass die Anzahl der fehlerbehafteten Größen deutlich geringer ist. 
Außerdem ist das Messen von Schwingungen nur durch die Reaktionszeit fehlerhaft und nicht abhängig von Geräten zur Aufnahme der Messwerte.
Dies ist besonders schwerwiegend bei dem Apparat, der die Striche auf dem Messstreifen gemacht hat.

\section{Anhang}
\label{sec:Anhang}

Plots der linearen Regression für die unterschiedlichen Massen, welche das Rad beschleunigen.

\begin{figure}[!h]
	\centering
	\input{pralle.tex}
	\caption{lineare Regression der Auslenkung gegen die Zeit der Massen\label{img:r100}}
\end{figure}

\begin{figure}[!hb]
	\centering
	\input{tischchenpolar.tex}
	\caption{Polardarstellung der reziproken Quadratwurzel des Tischchen\label{img:tischpol}}
\end{figure}
%\begin{figure}[!h]
%	\centering
%	\input{pr100.tex}
%	\caption{lineare Regression der Auslenkung gegen die Zeit der Masse 100g\label{img:r100}}
%\end{figure}
%
%\begin{figure}[!h]
%	\centering
%	\input{pr200.tex}
%	\caption{lineare Regression der Auslenkung gegen die Zeit der Masse 200g\label{img:r200}}
%\end{figure}
%
%\begin{figure}[!h]
%	\centering
%	\input{pr500.tex}
%	\caption{lineare Regression der Auslenkung gegen die Zeit der Masse 500g\label{img:r500}}
%\end{figure}
%
%\begin{figure}[!h]
%	\centering
%	\input{pr1000.tex}
%	\caption{lineare Regression der Auslenkung gegen die Zeit der Masse 1000g\label{img:r1000}}
%\end{figure}

C++ Programm zu auswerten und Messdaten in den globalen Variablen.\label{ag:prog}\\\\
\lstset{language=C++,breaklines=true, basicstyle=\ttfamily,showstringspaces=false, keywordstyle=\color{blue}\ttfamily, stringstyle=\color{red}\ttfamily, commentstyle=\color{green}\ttfamily, morecomment=[l][\color{magenta}]{\#}, tabsize=4}
\lstinputlisting{main.cpp}

\end{document}
