reset
set terminal epslatex color
#set terminal png
set output 'winkelfit.tex'
#set output 'winkelfit.png'

set key bottom right
set xlabel "Drehmoment [Nm]"
set ylabel "Auslenkung [rad]"
f(x)=m*x+b
g(x)=a*x+c

set fit  logfile 'winkelp.log'
fit f(x) "Winkel.txt" u 1:2:4 via m,b
set fit logfile 'winkelm.log'
fit g(x) "Winkel.txt" u 1:3:4 via a,c

p f(x) t "lineare Regression rechts" , "Winkel.txt" u 1:2:4 w e t "Auslenkung rechts" , g(x) lt -1 t "lineare Regression links" , "Winkel.txt" u 1:3:4 w e t "Auslenkung links"

set output
!epstopdf winkelfit.eps
