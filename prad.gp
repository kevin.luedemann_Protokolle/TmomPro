reset 
set terminal epslatex color
f(x)=m*x+b
g(x)=n*x+q
h(x)=v*x+w
k(x)=c*x+e
set xlabel 'Zeit [s]'
set ylabel 'Abstand Zeitmarke [m]'
set key right
set yrange [0:3]
set output 'pralle.tex'
set fit logfile 'pr200_log.txt'
fit f(x) "r200.txt" u 1:2:3 via m,b
set fit logfile 'pr500_log.txt'
fit g(x) "r500.txt" u 1:2:3 via n,q
set fit logfile 'pr1000_log.txt'
fit h(x) "r1000.txt" u 1:2:3 via v,w
set fit logfile 'pr100_log.txt'
fit k(x) "r100.txt" u 1:2:3 via c,e

p k(x) t 'lin. Regression 100g', "r100.txt" u 1:2:3 t 'Masse: 100g' w e , f(x) t 'lin. Regression 200g', "r200.txt" u 1:2:3 t 'Masse: 200g' w e, g(x) t 'lin. Regression 500g', "r500.txt" u 1:2:3 t 'Masse: 500g' w e, h(x) t 'lin. Regression 1000g', "r1000.txt" u 1:2:3 t 'Masse: 1000g' w e

set output
!epstopdf pralle.eps
